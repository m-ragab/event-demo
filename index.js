const config = require('config');
const winston = require('winston');
const express = require('express');
const app = express();

require('./startup/cors')(app);
require('./startup/morgan')(app);
require('./startup/helmet')(app);
require('./startup/passport')(app);
require('./startup/db')();
require('./startup/validation')();
require('./startup/routes')(app);

const port = process.env.PORT || config.get('port');
const server = app.listen(port, () => 
    winston.info(`Listening on port ${port}...`)
);

module.exports = server;
