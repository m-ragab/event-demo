const config = require('config');

module.exports = function(req, res, next) {
  if (req.user.role !== 'superAdmin') return res.status(403).send('Access denied.');
  if (req.user.role !== 'Admin') return res.status(401).send('You aren\'t authorized');

  next();
};
