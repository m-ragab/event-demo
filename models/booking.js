const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const bookingSchema = new Schema(
    {
        event: {
            type: Schema.Types.ObjectId,
            ref: 'Event'
        },
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User'
        }
    },
    { timestamps: true }
);

const Booking = mongoose.model('Booking', bookingSchema);

function validateBooking(booking) {
    const schema = {
      eventId: Joi.objectId().required(),
      userId: Joi.objectId().required()
    };
  
    return Joi.validate(movie, schema);
}

module.exports.Booking = Booking;
module.exports.validate = validateBooking;
