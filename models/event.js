const Joi = require('@hapi/joi');
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const eventSchema = new Schema({
    title: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 255
      },
      description: {
        type: String,
        required: true
      },
      price: {
        type: Number,
        required: true,
      },
      date: {
        type: Date,
        required: true
      },
      creator: {
          type: Schema.Types.ObjectId,
          ref: 'User'
      }
});

const Event = mongoose.model('Event', eventSchema);

function validateEvent(event) {
    const schema = {
        title: Joi.string()
        .min(5)
        .max(255)
        .required(),
        creatorId: Joi.objectId()
        .required(),
        description: Joi.string()
        .required(),
        price: Joi.number()
        .required(),
        date: Joi.date()
        .required()
    };
  
    return Joi.validate(event, schema);
}

module.exports.Event = Event;
module.exports.validate = validateEvent;