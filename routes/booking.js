const express = require('express');
const router = express.Router();
const User = require('../models/user');
const Event = require('../models/event');
const { Booking, validate } = require('../models/booking');
const admin = require('../middleware/admin');
const auth = require('../middleware/auth');
const validateObjectId = require('../middleware/validateObjectId');

router.get('/:id', [validateObjectId, auth], async (req, res) => {
    const bookings = await Booking.findById(req.body.eventId)
        .select('user');

    if (!bookings) return res.status(404).send('The event with the given ID was not found.');

    res.send(bookings);
});

router.get('/user/:id', [validateObjectId, auth], async (req, res) => {
    const bookings = await Booking.findById(req.body.userId)
        .select('event');

    if (!bookings) return res.status(404).send('The user with the given ID was not found.');

    res.send(bookings);
});


router.post('/',  [auth], async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const bookedEvent = await Event.findById(req.body.eventId);
    if (!bookedEvent) throw new Error('Event not found.');

    const user = await User.findById(req.body.userId);
    if (!user) throw new Error('User not found.');

    const booking = new Booking({
        event: bookedEvent,
        user: user
    });

    await booking.save();
    
    res.send(booking);

});

router.delete('/:id', async (req, res) => {
    const booking = await Booking.findByIdAndRemove(req.params.id);
    
    if (!booking) return res.status(404).send("The booking with the given ID was not found.");

    res.send(booking);
});


module.exports = router;