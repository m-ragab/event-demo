const express = require('express');
const router = express.Router();
const User = require('../models/user');
const { Event, validate } = require('../models/event');
const admin = require('../middleware/admin');
const auth = require('../middleware/auth');
const validateObjectId = require('../middleware/validateObjectId');

router.get('/', [auth], async (req, res) => {
  const events = await Event.find()
    .sort('name');
  res.send(events);
});


router.get('/upcoming', [auth], async (req, res) => {
const events = await Event.find({"$gt":{"$date":"$now"}})
    .sort('name');
  
  res.send(events);
});

router.get('/past', [auth], async (req, res) => {
  const events = await Event.find({"$lt":{"$date":"$now"}})
    .sort('name');

  res.send(events);
});

router.post('/', [auth, admin], async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const creator = await User.findById(req.body.creatorId);
  if (!creator) throw new Error('User not found.');

  let event = new Event({
    title: req.body.title,
    description: req.body.description,
    price: req.body.price,
    date: new Date(req.body.date),
    creator: {
      _id: creator._id,
      name: creator.name
    }
  });

  creator.createdEvents.push(event);
  await creator.save();

  res.send(event);
});


router.put('/:id', [validateObjectId, auth, admin], async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const event = await Event.findByIdAndUpdate(
    req.params.id,
    {
      title: req.body.title,
      description: req.body.description,
      price: req.body.price,
      date: req.body.date
    },
    { new: true }
  );

  if (!event) return res.status(404).send('The event with the given ID was not found.');

    res.send(event);
});

module.exports = router;