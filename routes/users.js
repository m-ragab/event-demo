const passport = require('passport');
const jwt = require('jsonwebtoken');
const express = require('express');
const router = express.Router();
const { User, validate} = require('../models/user');
const Event = require('../models/event');
const Booking = require('../models/booking');


router.post('/signup', passport.authenticate('signup', { session : false }) , async (req, res, next) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);
  
  res.json({ 
    message : 'Signup successful',
    user : req.user 
  });
});


router.post('/login', async (req, res, next) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

   passport.authenticate('login', async (err, user, info) => {     
     try {
          if(err || !user){
          const error = new Error('An Error occured')
          return next(error);
        }
       req.login(user, { session : false }, async (error) => {
         if( error ) return next(error)
         const body = { _id : user._id, email : user.email };
         const token = jwt.sign({ user : body },'top_secret');
         return res.json({ token });
       });     
      } 
      catch (error) {
       return next(error);
     }
   }) (req, res, next);
 });

 router.get('/:id', async (req, res) => {
  const bookings = await Booking.find(req.body.userId)
  .select('event');

  bookings = await bookings.find({"$lt":{"$date":"$now"}});

  res.send(events);
});

 module.exports = router;