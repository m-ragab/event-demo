const mongoose = require('mongoose');
const winston = require('winston');
const config = require('config');

module.exports = function() {
    const db = config.get('db');
    mongoose.connect(db, { useNewUrlParser: true })
        .then(() => {
            winston.info('MongoDB Connected.');
            winston.info(`Connected to: ${db}.`);
        })
        .catch((err) => {
            winston.info(err);
        });
}
