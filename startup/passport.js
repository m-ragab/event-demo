const passport = require('passport');
require('../routes/auth');

module.exports = function(app) {
    app.use(passport.initialize());
}