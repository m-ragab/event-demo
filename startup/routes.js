const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');
const users = require('../routes/users');
const secureRoute = require('../routes/secure-routes');
const events = require('../routes/events');
const booking = require('../routes/booking');
const error = require('../middleware/error');

module.exports = function(app) {
  app.use(express.json());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended : false }));
  app.use('/', users);
  app.use('/user', passport.authenticate('jwt', { session : false }), secureRoute );
  app.use('/events', events);
  app.use('/booking', booking);
  app.use(error);
}